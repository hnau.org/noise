package ru.hnau.noise

import android.content.Context
import android.opengl.GLSurfaceView
import ru.hnau.noise.renderer.NoiseRenderer


class NoiseView(
    context: Context,
    frameTimeCallback: (Long) -> Unit
) : GLSurfaceView(
    context
) {

    init {
        setEGLContextClientVersion(3)
        setRenderer(
            NoiseRenderer(
                context = context,
                frameTimeCallback = frameTimeCallback
            )
        )
        renderMode = RENDERMODE_CONTINUOUSLY
    }

}