package ru.hnau.noise

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import com.google.android.material.internal.ViewUtils.dpToPx

class MainActivity : AppCompatActivity() {

    private lateinit var noiseView: NoiseView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(
            FrameLayout(this).apply {
                val fpsView = TextView(context).apply {
                    setBackgroundColor(Color.argb(0.25f, 0f, 0f, 0f))
                    setPadding(dpToPx(context, 8).toInt())
                    setTextColor(Color.WHITE)
                    setTextSize(24f)
                    text = "0 FPS"
                    layoutParams = FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    ).apply {
                        gravity = Gravity.END or Gravity.BOTTOM
                    }
                }
                var lastFPSUpdate = System.currentTimeMillis()
                var fpsSum = 0f
                var fpsCount = 0
                noiseView = NoiseView(
                    context = context,
                    frameTimeCallback = { delta ->
                        val now = System.currentTimeMillis()
                        if (now > lastFPSUpdate + 500L && fpsCount > 0) {
                            val avgFps = fpsSum / fpsCount
                            post { fpsView.text = "${avgFps.toInt()} FPS" }
                            fpsCount = 0
                            fpsSum = 0f
                            lastFPSUpdate = now
                        } else {
                            fpsSum += 1000f / delta;
                            fpsCount++;
                        }
                    }
                )
                addView(noiseView)
                addView(fpsView)
            }
        )
    }

    override fun onResume() {
        super.onResume()
        noiseView.onResume()
    }

    override fun onPause() {
        super.onPause()
        noiseView.onPause()
    }

}