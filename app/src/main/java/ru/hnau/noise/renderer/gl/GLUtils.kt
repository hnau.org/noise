package ru.hnau.noise.renderer.gl

import android.content.Context
import android.opengl.GLES20


object GLUtils {

    fun readAssert(
        context: Context,
        filename: String
    ) = context
        .assets
        .open(filename)
        .reader()
        .readText()

    fun compileShader(
        code: String,
        type: Int
    ): Int {
        // Load in the shader
        val shaderHandle = GLES20.glCreateShader(type)
        if (shaderHandle == 0) throw GLInitializationException("Error while creating shader with type: $type")

        // Pass in the shader source
        GLES20.glShaderSource(shaderHandle, code)
        // Compile the shader
        GLES20.glCompileShader(shaderHandle)

        // Get the compilation status
        val compileStatus = IntArray(1)
        GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0)
        // If the compilation failed, delete the shader.
        if (compileStatus[0] == 0) {
            GLES20.glDeleteShader(shaderHandle)
            throw GLInitializationException("Error while compiling shader: ${GLES20.glGetShaderInfoLog(shaderHandle)}")
        }

        return shaderHandle
    }

    fun prepareProgram(
        vertexShaderCode: String,
        fragmentShaderCode: String
    ): Int {

        // Create a program object and store the handle to it
        val programHandle = GLES20
            .glCreateProgram()
            .takeIf { it > 0 }
            ?: throw GLInitializationException("Error while creating program")

        val vertexShader = compileShader(vertexShaderCode, GLES20.GL_VERTEX_SHADER)
        val fragmentShader = compileShader(fragmentShaderCode, GLES20.GL_FRAGMENT_SHADER)

        // Bind the vertex shader to the program
        GLES20.glAttachShader(programHandle, vertexShader)
        // Bind the fragment shader to the program
        GLES20.glAttachShader(programHandle, fragmentShader)
        // Link two shaders together into a program
        GLES20.glLinkProgram(programHandle)

        // Get the link status
        val linkStatus = IntArray(1)
        GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0)
        // If the link failed, delete the program
        if (linkStatus[0] <= 0) {
            throw GLInitializationException("Error while building program: ${GLES20.glGetProgramInfoLog(programHandle)}")
        }

        GLES20.glDetachShader(programHandle, vertexShader)
        GLES20.glDetachShader(programHandle, fragmentShader)

        GLES20.glDeleteShader(vertexShader)
        GLES20.glDeleteShader(fragmentShader)

        // Tell OpenGL to use this program when rendering
        GLES20.glUseProgram(programHandle)

        return programHandle
    }

}