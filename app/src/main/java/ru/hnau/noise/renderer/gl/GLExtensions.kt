package ru.hnau.noise.renderer.gl

import android.opengl.GLES20
import android.util.Log

private const val TAG = "GLExtensions"

fun logIfGlError(tag: String) {
    var error: Int
    while (GLES20.glGetError().also { error = it } != GLES20.GL_NO_ERROR) {
        Log.w(TAG, "glError[$tag]: $error")
    }
}