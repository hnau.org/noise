package ru.hnau.noise

import java.lang.IllegalStateException


interface ProgramPointer {

    val programPointer: Int

}

class LateinitProgramPointer : ProgramPointer {

    private var programPointerInner: Int? = null

    override val programPointer: Int
        get() = programPointerInner
            ?: throw IllegalStateException("Program pointer was not initialized")

    fun initialize(
        programPointer: Int
    ) {
        programPointerInner = programPointer
    }

}