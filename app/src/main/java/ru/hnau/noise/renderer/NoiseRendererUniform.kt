package ru.hnau.noise.renderer

import ru.hnau.noise.ProgramPointer
import ru.hnau.noise.renderer.gl.uniform1F
import ru.hnau.noise.renderer.gl.uniform1I
import ru.hnau.noise.renderer.gl.uniform2F
import ru.hnau.noise.renderer.gl.uniform3F
import ru.hnau.noise.renderer.gl.uniform3FV


class NoiseRendererUniform(
    programPointer: ProgramPointer
) {

    var screenSize
            by programPointer.uniform2F("screenSize")

    var offset
            by programPointer.uniform1F("offset")

    var colors
            by programPointer.uniform3FV("colors")

    var colorsCount
            by programPointer.uniform1I("colorsCount")

    var colorsOffsetFactor
            by programPointer.uniform1F("colorsOffsetFactor")

    var wavesOffsetFactors
            by programPointer.uniform3F("wavesOffsetFactors")

    var wavesScale
            by programPointer.uniform1F("wavesScale")

    var wavesDispersionFactors
            by programPointer.uniform2F("wavesDispersionFactors")

    var wavesCount
            by programPointer.uniform1F("wavesCount")

    var wavesPower
            by programPointer.uniform1F("wavesPower")

}