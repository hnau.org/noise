package ru.hnau.noise.renderer.gl

import java.lang.Exception


class GLInitializationException(message: String) : Exception(message)