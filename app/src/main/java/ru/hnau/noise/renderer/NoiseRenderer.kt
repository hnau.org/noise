package ru.hnau.noise.renderer

import android.content.Context
import android.graphics.Color
import android.opengl.GLES31
import android.opengl.GLSurfaceView
import ru.hnau.noise.renderer.gl.GLUtils
import ru.hnau.noise.LateinitProgramPointer
import java.nio.ByteBuffer
import java.nio.ByteOrder
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


class NoiseRenderer(
    private val context: Context,
    private val noiseConfig: NoiseConfig = NoiseConfig.default,
    private val frameTimeCallback: (Long) -> Unit
) : GLSurfaceView.Renderer {

    private val programPointer = LateinitProgramPointer()

    private val uniform = NoiseRendererUniform(programPointer)

    private val start = System.currentTimeMillis()

    private var previousFrameTimestamp: Long? = null

    override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
        GLES31.glClearColor(0f, 1f, 0f, 1f);

        programPointer.initialize(
            GLUtils.prepareProgram(
                vertexShaderCode = GLUtils.readAssert(
                    context = context,
                    filename = "shader/noise.vert"
                ),
                fragmentShaderCode = GLUtils.readAssert(
                    context = context,
                    filename = "shader/noise.frag"
                )
            )
        )

        setFaces()

        uniform.wavesScale = 0.000075f
        uniform.wavesCount = 20f
        uniform.wavesDispersionFactors = 0.3f to 0f
        uniform.wavesPower = 2f

        uniform.colorsOffsetFactor = 1f
        uniform.wavesOffsetFactors = Triple(0.1f, 0f, 0.02f)

        uniform.colorsCount = noiseConfig.colors.size
        uniform.colors = noiseConfig.colors.map { color ->
            Triple(
                Color.red(color) / 255f,
                Color.green(color) / 255f,
                Color.blue(color) / 255f
            )
        }
    }

    private fun setFaces() {
        val vertexesAttributeLocation = GLES31.glGetAttribLocation(
            programPointer.programPointer,
            "a_position"
        )
        GLES31.glVertexAttribPointer(
            vertexesAttributeLocation,
            2,
            GLES31.GL_FLOAT,
            false,
            0,
            listOf(-1 to -1, 1 to -1, -1 to 1, 1 to 1)
                .flatMap(Pair<Int, Int>::toList)
                .map(Int::toFloat)
                .toFloatArray()
                .let { floats ->
                    ByteBuffer
                        .allocateDirect(floats.size * Float.SIZE_BYTES)
                        .order(ByteOrder.nativeOrder())
                        .asFloatBuffer()
                        .put(floats)
                        .apply { position(0) }
                }

        )
        GLES31.glEnableVertexAttribArray(vertexesAttributeLocation)
    }

    override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
        GLES31.glViewport(0, 0, width, height)
        uniform.screenSize = width.toFloat() to height.toFloat()
    }

    override fun onDrawFrame(gl: GL10) {
        val now = System.currentTimeMillis()
        GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT or GLES31.GL_DEPTH_BUFFER_BIT)
        val timeOffset = (now - start).toFloat()
        uniform.offset = timeOffset * noiseConfig.speed / 1000f
        GLES31.glDrawArrays(GLES31.GL_TRIANGLE_STRIP, 0, 4)
        previousFrameTimestamp?.let { previousFrame ->
            val delta = now - previousFrame
            frameTimeCallback(delta)
        }
        previousFrameTimestamp = now
    }

}