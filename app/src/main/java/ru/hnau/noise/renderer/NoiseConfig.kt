package ru.hnau.noise.renderer

import android.graphics.Color


data class NoiseConfig(
    val colors: List<Int>,
    val speed: Float = 1f
) {

    companion object {

        val default = NoiseConfig(
            colors = listOf(
                "#FF1C6B",
                "#FD9D51",
                "#B712C4"
            ).map(Color::parseColor)
        )

    }

}