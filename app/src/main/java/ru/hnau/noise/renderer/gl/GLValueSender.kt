package ru.hnau.noise.renderer.gl

import android.opengl.GLES31
import ru.hnau.noise.ProgramPointer
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


typealias GLLocationExtractor = (program: Int, key: String) -> Int
typealias GLValueSetter<T> = (location: Int, value: T) -> Unit

fun <T> ProgramPointer.GLValueSender(
    key: String,
    extractLocation: GLLocationExtractor,
    setter: GLValueSetter<T>
) = object : ReadWriteProperty<Any?, T> {

    var programPointerWithLocation: Pair<Int, Int>? = null

    override fun getValue(
        thisRef: Any?,
        property: KProperty<*>
    ): T = throw NotImplementedError(
        "Getting value from GL program not implemented"
    )

    override fun setValue(
        thisRef: Any?,
        property: KProperty<*>,
        value: T
    ) {
        val actualProgramPointer = this@GLValueSender.programPointer
        val location = programPointerWithLocation
            ?.takeIf { (cachedLocationProgramPointer) ->
                cachedLocationProgramPointer == actualProgramPointer
            }
            ?.second
            ?: run {
                val location = extractLocation(actualProgramPointer, key)
                programPointerWithLocation = actualProgramPointer to location
                location
            }
        setter(location, value)
    }

}

private val attributeLocationExtractor: GLLocationExtractor = GLES31::glGetAttribLocation
private val uniformLocationExtractor: GLLocationExtractor = GLES31::glGetUniformLocation

fun ProgramPointer.uniform1I(
    key: String
) = GLValueSender(
    key = key,
    extractLocation = uniformLocationExtractor,
    setter = GLES31::glUniform1i
)

fun ProgramPointer.uniform1F(
    key: String
) = GLValueSender(
    key = key,
    extractLocation = uniformLocationExtractor,
    setter = GLES31::glUniform1f
)

fun ProgramPointer.uniform2F(
    key: String
) = GLValueSender<Pair<Float, Float>>(
    key = key,
    extractLocation = uniformLocationExtractor,
    setter = { location, (f1, f2) ->
        GLES31.glUniform2f(location, f1, f2)
    }
)

fun ProgramPointer.uniform3F(
    key: String
) = GLValueSender<Triple<Float, Float, Float>>(
    key = key,
    extractLocation = uniformLocationExtractor,
    setter = { location, (f1, f2, f3) ->
        GLES31.glUniform3f(location, f1, f2, f3)
    }
)

fun ProgramPointer.uniform3FV(
    key: String
) = GLValueSender<List<Triple<Float, Float, Float>>>(
    key = key,
    extractLocation = uniformLocationExtractor,
    setter = { location, values ->
        GLES31.glUniform3fv(
            location,
            values.size,
            values.flatMap(Triple<Float, Float, Float>::toList).toFloatArray(),
            0
        )
    }
)