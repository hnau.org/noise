precision highp float;

#define maxColorsCount 16

uniform vec2 screenSize;
uniform float offset;

uniform vec3 colors[maxColorsCount];
uniform int colorsCount;
uniform float colorsOffsetFactor;

uniform vec3 wavesOffsetFactors;
uniform float wavesScale;
uniform vec2 wavesDispersionFactors;
uniform float wavesCount;
uniform float wavesPower;

float random1(float offset, int index) {
    offset += 2048.0;
    offset *= float(index);
    float result = sin(offset);
    if (result < 0.0) {
        result = -result;
    }
    return result;
}

float noise1(float offset, int index) {
    float beforeToAfter = fract(offset);
    float beforeOffset = offset - beforeToAfter;
    float before = random1(beforeOffset, index);
    float after = random1(beforeOffset+1.0, index);
    return mix(before, after, beforeToAfter);
}

vec3 calcColor(vec2 position) {
    float aspectRatio = screenSize.y / screenSize.x;
    float maxDistance = distance(vec2(0.0, 0.0), screenSize);
    float factorsSum = 0.0;
    float distancesToColors[maxColorsCount];
    float maxDistanceToColor = 0.0;
    float colorsOffset = offset * colorsOffsetFactor;
    for (int i = 0; i < colorsCount; i++) {
        float colorX = noise1(colorsOffset, i*2) * screenSize.x;
        float colorY = noise1(colorsOffset/aspectRatio, i*2+1) * screenSize.y;
        float distance = distance(position, vec2(colorX, colorY));
        distancesToColors[i] = distance;
        if (distance > maxDistanceToColor){
            maxDistanceToColor = distance;
        }
    }
    vec3 result = vec3(0.0, 0.0, 0.0);
    if (maxDistanceToColor <= 0.0) {
        return result;
    }
    for (int i = 0; i < colorsCount; i++) {
        float factor = distancesToColors[i] / maxDistanceToColor;
        factor *= factor;
        factor *= factor;
        factorsSum += factor;
        result += colors[i] * factor;
    }
    if (factorsSum > 0.0) {
        result /= factorsSum;
    }
    return result;
}

/* discontinuous pseudorandom uniformly distributed in [-0.5, +0.5]^3 */
vec3 random3(vec3 c) {
    float j = 4096.0*sin(dot(c, vec3(17.0, 59.4, 15.0)));
    vec3 r;
    r.z = fract(512.0*j);
    j *= .125;
    r.x = fract(512.0*j);
    j *= .125;
    r.y = fract(512.0*j);
    return r-0.5;
}

const float F3 =  0.3333333;
const float G3 =  0.1666667;
float snoise(vec3 p) {

    vec3 s = floor(p + dot(p, vec3(F3)));
    vec3 x = p - s + dot(s, vec3(G3));

    vec3 e = step(vec3(0.0), x - x.yzx);
    vec3 i1 = e*(1.0 - e.zxy);
    vec3 i2 = 1.0 - e.zxy*(1.0 - e);

    vec3 x1 = x - i1 + G3;
    vec3 x2 = x - i2 + 2.0*G3;
    vec3 x3 = x - 1.0 + 3.0*G3;

    vec4 w, d;

    w.x = dot(x, x);
    w.y = dot(x1, x1);
    w.z = dot(x2, x2);
    w.w = dot(x3, x3);

    w = max(0.6 - w, 0.0);

    d.x = dot(random3(s), x);
    d.y = dot(random3(s + i1), x1);
    d.z = dot(random3(s + i2), x2);
    d.w = dot(random3(s + 1.0), x3);

    w *= w;
    w *= w;
    d *= w;

    return dot(d, vec4(52.0));
}

vec2 calcWavesDispersion() {
    vec3 baseOffset =  vec3(offset) * wavesOffsetFactors;
    vec3 coordinatesOffset = vec3(gl_FragCoord.xy * wavesScale, 0.0);
    float noise = snoise(baseOffset + coordinatesOffset);
    noise = fract(noise * wavesCount);
    noise = pow(noise, wavesPower);
    noise *= 2.0;
    if (noise > 1.0) {
        noise = 2.0 - noise;
    }
    if (noise < -1.0) {
        noise = -2.0 - noise;
    }
    vec2 dispersion = screenSize * wavesDispersionFactors * noise;
    return dispersion;
}

void main() {
    vec2 dispersion = calcWavesDispersion();
    vec3 color = calcColor(gl_FragCoord.xy + dispersion);
    gl_FragColor = vec4(color, 1.0);
}